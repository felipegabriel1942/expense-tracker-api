package br.com.felipegabriel.expensetrackerapi.listener;

import br.com.felipegabriel.expensetrackerapi.model.entity.Audit;
import br.com.felipegabriel.expensetrackerapi.model.entity.Auditable;
import br.com.felipegabriel.expensetrackerapi.util.TokenUtils;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

import java.time.LocalDateTime;

public class AuditListener {

    @PrePersist
    public void setCreateOn(Auditable auditable) {
        Audit audit = auditable.getAudit();

        if (audit == null) {
            audit = new Audit();
            auditable.setAudit(audit);
        }

        audit.setUser(TokenUtils.getUserEmail());
        audit.setCreationDate(LocalDateTime.now());
        audit.setUpdateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void setUpdateOn(Auditable auditable) {
        Audit audit = auditable.getAudit();
        audit.setUpdateDate(LocalDateTime.now());
    }
}
