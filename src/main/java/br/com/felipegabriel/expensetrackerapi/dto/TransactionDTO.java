package br.com.felipegabriel.expensetrackerapi.dto;

import br.com.felipegabriel.expensetrackerapi.enums.PeriodEnum;
import br.com.felipegabriel.expensetrackerapi.model.entity.Audit;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@Setter @Getter
@NoArgsConstructor @AllArgsConstructor
public class TransactionDTO {

    private Long id;

    private String description;

    private BigDecimal value;

    private LocalDate transactionDate;

    private TransactionCategoryDTO transactionCategory;

    private Integer installment;

    private boolean isFixed;

    private Integer totalInstallments;

    @Enumerated(EnumType.STRING)
    private PeriodEnum period;

    private Audit audit;
}
