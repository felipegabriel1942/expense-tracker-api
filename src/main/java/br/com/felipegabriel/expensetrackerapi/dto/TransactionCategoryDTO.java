package br.com.felipegabriel.expensetrackerapi.dto;
import br.com.felipegabriel.expensetrackerapi.enums.TransactionTypeEnum;
import lombok.*;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

@Getter @Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionCategoryDTO {

    private Long id;

    private String description;

    @Enumerated(EnumType.STRING)
    private TransactionTypeEnum transactionType;
}
