package br.com.felipegabriel.expensetrackerapi.dto;

import br.com.felipegabriel.expensetrackerapi.util.TokenUtils;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter @Getter
public class TransactionParamsDTO {
    private Integer page;
    private Integer elementsPerPage;
    private boolean expense;
    private boolean revenue;
    private String period;
    private String user = TokenUtils.getUserEmail();
}
