package br.com.felipegabriel.expensetrackerapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserRegistrationDTO {

    private String email;

    private String password;

    private String firstName;

    private String lastName;
}
