package br.com.felipegabriel.expensetrackerapi.controller;

import br.com.felipegabriel.expensetrackerapi.dto.*;
import br.com.felipegabriel.expensetrackerapi.enums.MessageEnum;
import br.com.felipegabriel.expensetrackerapi.service.TransactionService;
import br.com.felipegabriel.expensetrackerapi.util.ResponseUtils;

import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/transaction")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;

    @PostMapping("create")
    public ResponseEntity<Void> save(@RequestBody TransactionDTO transactionDTO) {
        transactionService.save(transactionDTO);
        return new ResponseUtils().ok(MessageEnum.TRANSACTION_SAVED);
    }

    @DeleteMapping("delete")
    public ResponseEntity<Void> delete(@RequestParam("id") Integer id) {
        transactionService.delete(id);
        return new ResponseUtils().ok(MessageEnum.TRANSACTION_DELETED);
    }

    @DeleteMapping("delete-future-transactions")
    public ResponseEntity<Void> deleteFutureTransactions(@RequestParam("id") Integer id) {
        transactionService.deleteFutureTransactions(id);
        return new ResponseUtils().ok(MessageEnum.TRANSACTION_DELETED);
    }

    @DeleteMapping("delete-connected-transactions")
    public ResponseEntity<Void> deleteConnectedTransactions(@RequestParam("id") Integer id) {
        transactionService.deleteConnectedTransactions(id);
        return new ResponseUtils().ok(MessageEnum.TRANSACTION_DELETED);
    }

    @PutMapping("update")
    public ResponseEntity<Void> update(@RequestBody TransactionDTO transactionDTO) {
        transactionService.update(transactionDTO);
        return new ResponseUtils().ok(MessageEnum.TRANSACTION_UPDATED);
    }

    @GetMapping("find")
    public ResponseEntity<ApiResponseDTO<PageDTO<TransactionDTO>>> findTransactions(
            TransactionParamsDTO transactionParams
    ) {
        PageDTO<TransactionDTO> transactions = transactionService.findTransactions(transactionParams);
        return new ResponseUtils().ok(transactions);
    }

    // TODO: Criar um controller para os metodos abaixo
    @GetMapping("summary")
    public ResponseEntity<ApiResponseDTO<List<TransactionSummaryDTO>>> getSummary(
            TransactionParamsDTO transactionParams) {
        List<TransactionSummaryDTO> summary = transactionService.getTransactionSummary(transactionParams);
        return new ResponseUtils().ok(summary);
    }

    @GetMapping("summary/expenses")
    public ResponseEntity<ApiResponseDTO<List<ExpensesSummaryDTO>>> getExpenseSummary(
            TransactionParamsDTO transactionParams) {
        List<ExpensesSummaryDTO> summary = transactionService.getExpenseSummaryByCategory(transactionParams);
        return new ResponseUtils().ok(summary);
    }
}
