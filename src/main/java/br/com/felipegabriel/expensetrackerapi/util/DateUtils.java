package br.com.felipegabriel.expensetrackerapi.util;

import br.com.felipegabriel.expensetrackerapi.enums.PeriodEnum;

import java.time.LocalDate;

public class DateUtils {

    public static LocalDate addPeriodOfTime(LocalDate date, PeriodEnum period, Integer timeToAdd) {

        if (period.equals(PeriodEnum.DAILY)) {
            date = date.plusDays(timeToAdd);
        }

        if (period.equals(PeriodEnum.WEEKLY)) {
            date = date.plusWeeks(timeToAdd);
        }

        if (period.equals(PeriodEnum.MONTHLY)) {
            date = date.plusMonths(timeToAdd);
        }

        if (period.equals(PeriodEnum.YEARLY)) {
            date = date.plusYears(timeToAdd);
        }

        return date;
    }
}
