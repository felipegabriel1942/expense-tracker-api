package br.com.felipegabriel.expensetrackerapi.util;

import br.com.felipegabriel.expensetrackerapi.model.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class TokenUtils {

    public static String getUserEmail() {
        Authentication authToken = SecurityContextHolder.getContext().getAuthentication();

        if (authToken == null) {
            return "";
        }

        return ((User) authToken.getPrincipal()).getEmail();
    }
}
