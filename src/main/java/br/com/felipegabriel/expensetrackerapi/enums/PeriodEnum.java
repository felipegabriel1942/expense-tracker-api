package br.com.felipegabriel.expensetrackerapi.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PeriodEnum {

    MONTHLY("Mensal"), DAILY("Diário"), WEEKLY("Semanal"), YEARLY("Anual");

    private String value;

    PeriodEnum(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static PeriodEnum fromValue(String value) {
        if (PeriodEnum.DAILY.getValue().equals(value)) {
            return PeriodEnum.DAILY;
        }

        if (PeriodEnum.WEEKLY.getValue().equals(value)) {
            return PeriodEnum.WEEKLY;
        }

        if (PeriodEnum.MONTHLY.getValue().equals(value)) {
            return PeriodEnum.MONTHLY;
        }

        if (PeriodEnum.YEARLY.getValue().equals(value)) {
            return PeriodEnum.YEARLY;
        }

        throw new IllegalArgumentException();
    }
}
