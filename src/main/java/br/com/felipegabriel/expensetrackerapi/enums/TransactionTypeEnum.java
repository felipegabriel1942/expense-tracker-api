package br.com.felipegabriel.expensetrackerapi.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TransactionTypeEnum {
    REVENUE("Receita"),
    EXPENSE("Despesa");

    private String value;

    TransactionTypeEnum(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static TransactionTypeEnum fromValue(String value) {
        if (TransactionTypeEnum.EXPENSE.getValue().equals(value) ) {
            return TransactionTypeEnum.EXPENSE;
        }

        if (TransactionTypeEnum.REVENUE.getValue().equals(value) ) {
            return TransactionTypeEnum.REVENUE;
        }

        throw new IllegalArgumentException();
    }
}
