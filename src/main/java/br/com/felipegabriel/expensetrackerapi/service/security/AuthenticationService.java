package br.com.felipegabriel.expensetrackerapi.service.security;

import br.com.felipegabriel.expensetrackerapi.dto.UserAuthenticationDTO;
import br.com.felipegabriel.expensetrackerapi.dto.UserRegistrationDTO;
import br.com.felipegabriel.expensetrackerapi.model.entity.User;
import br.com.felipegabriel.expensetrackerapi.model.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;


    public void register(UserRegistrationDTO userRegistrationDTO) {
        User user = User.builder()
                .email(userRegistrationDTO.getEmail())
                .password(passwordEncoder.encode(userRegistrationDTO.getPassword()))
                .firstName(userRegistrationDTO.getFirstName())
                .lastName(userRegistrationDTO.getLastName())
                .build();

        userRepository.save(user);
    }

    public String authenticate(UserAuthenticationDTO userAuthenticationDTO) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userAuthenticationDTO.getEmail(),
                        userAuthenticationDTO.getPassword()
                )
        );

        User user = userRepository.findByEmail(userAuthenticationDTO.getEmail())
                .orElseThrow();

        return jwtService.generateToken(user);
    }

}
