package br.com.felipegabriel.expensetrackerapi.service;

import br.com.felipegabriel.expensetrackerapi.dto.*;
import br.com.felipegabriel.expensetrackerapi.exception.TransactionNotFound;
import br.com.felipegabriel.expensetrackerapi.mapper.TransactionMapper;
import br.com.felipegabriel.expensetrackerapi.model.entity.Transaction;
import br.com.felipegabriel.expensetrackerapi.model.repository.TransactionRepository;

import br.com.felipegabriel.expensetrackerapi.util.DateUtils;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@AllArgsConstructor
@Slf4j
public class TransactionService {

    private final TransactionRepository transactionRepository;

    private final TransactionMapper transactionMapper;

    @Transactional
    public void save(TransactionDTO transactionDTO) {
        Transaction savedTransaction = transactionRepository.save(transactionMapper.toEntity(transactionDTO));

        log.info("CREATED: " + savedTransaction);

        if (hasFutureTransactions(savedTransaction)) {
            List<Transaction> futureTransactions = buildFutureTransactions(savedTransaction);
            List<Transaction> savedFutureTransactions = transactionRepository.saveAll(futureTransactions);
            savedFutureTransactions.forEach(transaction -> log.info("CREATED: " + transaction));
        }
    }

    private boolean hasFutureTransactions(Transaction transaction) {
        return transaction.getTotalInstallments() > 1;
    }

    private List<Transaction> buildFutureTransactions(Transaction transaction) {
        return IntStream
                .range(0, transaction.getTotalInstallments() - 1)
                .mapToObj(i -> {
                    Transaction futureTransaction = SerializationUtils.clone(transaction);
                    futureTransaction.setInstallment(i + 2);
                    futureTransaction.setId(null);
                    futureTransaction.setTransactionDate(DateUtils.addPeriodOfTime(
                            transaction.getTransactionDate(),
                            transaction.getPeriod(),
                            i + 1
                    ));

                    futureTransaction.setMainTransactionId(transaction.getId());
                    return futureTransaction;
                })
                .collect(Collectors.toList());
    }

    public void deleteFutureTransactions(Integer id) {
        List<Transaction> futureTransactions = transactionRepository.findFutureTransactions(id);
        transactionRepository.deleteAll(futureTransactions);
        futureTransactions.forEach(transaction -> log.info("DELETED: " + transaction));
    }

    public void deleteConnectedTransactions(Integer id) {
        List<Transaction> futureTransactions = transactionRepository.findConnectedTransactions(id);
        transactionRepository.deleteAll(futureTransactions);
        futureTransactions.forEach(transaction -> log.info("DELETED: " + transaction));
    }

    public void delete(Integer id) {
        Optional<Transaction> transaction = transactionRepository.findById(id);

        if (transaction.isEmpty()) {
            throw new TransactionNotFound();
        }

        transactionRepository.deleteById(id);
        log.info("DELETED: " + transaction.get());
    }

    public void update(TransactionDTO transactionDTO) {
        if (Objects.isNull(transactionDTO.getId())) {
            throw new TransactionNotFound();
        }

        Transaction transaction = transactionMapper.toEntity(transactionDTO);

        transactionRepository.save(transaction);

        log.info("UPDATED: " + transaction);
    }

    public PageDTO<TransactionDTO> findTransactions(TransactionParamsDTO params) {
        Pageable pageable = PageRequest.of(params.getPage(), params.getElementsPerPage());
        Page<Transaction> page = transactionRepository.findTransactions(params, pageable);
        List<TransactionDTO> transactions = transactionMapper.toListDto(page.getContent());
        return new PageDTO<TransactionDTO>().map(page, transactions);
    }

    // TODO: Criar um serviço para os metodos abaixo
    public List<TransactionSummaryDTO> getTransactionSummary(TransactionParamsDTO params) {
        return transactionRepository.getTransactionSummaryByPeriod(params);
    }

    public List<ExpensesSummaryDTO> getExpenseSummaryByCategory(TransactionParamsDTO params) {
        return transactionRepository.getExpenseSummaryByCategory(params);
    }
}
