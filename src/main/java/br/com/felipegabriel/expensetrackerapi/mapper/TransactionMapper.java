package br.com.felipegabriel.expensetrackerapi.mapper;

import br.com.felipegabriel.expensetrackerapi.dto.TransactionDTO;
import br.com.felipegabriel.expensetrackerapi.model.entity.Transaction;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    TransactionDTO toDto(Transaction transaction);

    Transaction toEntity(TransactionDTO transaction);

    List<TransactionDTO> toListDto(List<Transaction> transactions);
}
