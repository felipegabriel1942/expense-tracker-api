package br.com.felipegabriel.expensetrackerapi.model.query;

public abstract class TransactionQuery {

    public static final String FIND_TRANSACTIONS = "select t from Transaction t " +
            "where " +
            "((t.transactionCategory.transactionType = 'REVENUE' and :#{#params.revenue} = true) or (t.transactionCategory.transactionType = 'EXPENSE' and :#{#params.expense} = true)) " +
            "and t.audit.user = :#{#params.user} " +
            "and concat(year(t.transactionDate), '-', month(t.transactionDate)) = :#{#params.period} " +
            "order by t.id desc ";

    public static final String GET_TRANSACTION_SUMMARY = "select new br.com.felipegabriel.expensetrackerapi.dto.TransactionSummaryDTO(" +
            "   t.transactionCategory.transactionType, " +
            "   sum(t.value)) " +
            " from Transaction t " +
            " where" +
            "  concat(year(t.transactionDate), '-', month(t.transactionDate)) = :#{#params.period} " +
            "  and t.audit.user = :#{#params.user}" +
            " group by" +
            "  t.transactionCategory.transactionType";

    public static final String GET_EXPENSES_SUMMARY_BY_CATEGORY  = "select new br.com.felipegabriel.expensetrackerapi.dto.ExpensesSummaryDTO(" +
            "   t.transactionCategory.description, " +
            "   sum(t.value)) " +
            " from Transaction t " +
            " where" +
            "  concat(year(t.transactionDate), '-', month(t.transactionDate)) = :#{#params.period} " +
            "  and t.transactionCategory.transactionType = 'EXPENSE' " +
            "  and t.audit.user = :#{#params.user} " +
            " group by" +
            "  t.transactionCategory.description";

    public static final String FIND_FUTURE_TRANSACTIONS = "select t from Transaction t " +
            "where " +
            " t.installment > ( " +
            "   select t.installment " +
            "   from Transaction t " +
            "   where " +
            "       t.id = ?1" +
            " ) and t.mainTransactionId = ( " +
            "   select " +
            "       case when t.mainTransactionId is null then t.id else t.mainTransactionId end as mainTransactionId" +
            "   from Transaction t " +
            "   where " +
            "       t.id = ?1" +
            " )";

    public static final String FIND_CONNECTED_TRANSACTIONS = "select t from Transaction t " +
            " where " +
            "   t.id = ( " +
            "       select " +
            "           case when t.mainTransactionId is null then t.id else t.mainTransactionId end as mainTransactionI " +
            "       from Transaction t " +
            "       where " +
            "           t.id = ?1 " +
            "   ) or t.mainTransactionId = ( " +
            "       select " +
            "           case when t.mainTransactionId is null then t.id else t.mainTransactionId end as mainTransactionI " +
            "       from Transaction t " +
            "       where " +
            "           t.id = ?1 " +
            "   )";
}
