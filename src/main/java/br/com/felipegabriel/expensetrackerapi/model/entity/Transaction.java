package br.com.felipegabriel.expensetrackerapi.model.entity;

import br.com.felipegabriel.expensetrackerapi.enums.PeriodEnum;
import br.com.felipegabriel.expensetrackerapi.listener.AuditListener;
import lombok.*;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "_transaction")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditListener.class)
@ToString
public class Transaction implements Auditable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private BigDecimal value;

    @Column(name = "transaction_date")
    private LocalDate transactionDate;

    @ManyToOne
    @JoinColumn(name = "transaction_category")
    private TransactionCategory transactionCategory;

    @Column(name = "is_fixed")
    private boolean isFixed;

    private Integer installment;

    @Column(name = "total_installments")
    private Integer totalInstallments;

    @Enumerated(EnumType.STRING)
    private PeriodEnum period;

    @Column(name = "main_transaction_id")
    private Long mainTransactionId;

    @Embedded
    private Audit audit;

}
