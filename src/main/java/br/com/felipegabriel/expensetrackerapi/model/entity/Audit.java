package br.com.felipegabriel.expensetrackerapi.model.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Embeddable
@Data
public class Audit implements Serializable {

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "user_email")
    private String user;

}
