package br.com.felipegabriel.expensetrackerapi.model.entity;

import br.com.felipegabriel.expensetrackerapi.enums.TransactionTypeEnum;
import lombok.*;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "transaction_category")
@Setter @Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name ="transaction_type")
    private TransactionTypeEnum transactionType;
}
