package br.com.felipegabriel.expensetrackerapi.model.entity;

public interface Auditable {

    Audit getAudit();

    void setAudit(Audit audit);
}
