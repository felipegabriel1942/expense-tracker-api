create table _user (
	id serial primary key,
	email varchar(100) unique not null,
	secret varchar(255) not null,
	first_name varchar(100) not null,
	last_name varchar(100) not null
);

create table transaction_category (
	id serial primary key,
	description varchar(255) not null,
	transaction_type varchar(10) not null
);

create table _transaction (
	id serial primary key,
	description varchar(255) not null,
	value numeric(32,2) not null,
	transaction_date date not null,
	transaction_category bigint not null,
	is_fixed boolean not null default false,
	installment bigint not null default 1,
	total_installments bigint not null default 1,
	period varchar(20) not null,
	main_transaction_id bigint,
	creation_date timestamp not null,
	update_date timestamp not null,
	user_email varchar(100) not null,
	constraint fk_transaction_type foreign key (transaction_category) references transaction_category (id)
);

insert into transaction_category(description, transaction_type)
values
	('Salário', 'REVENUE'),
	('Prêmios', 'REVENUE'),
	('Investimentos', 'REVENUE'),
	('Alimentação', 'EXPENSE'),
	('Lazer', 'EXPENSE'),
	('Educação', 'EXPENSE'),
	('Moradia', 'EXPENSE'),
	('Pagamentos', 'EXPENSE'),
	('Vestuário', 'EXPENSE'),
	('Saúde', 'EXPENSE'),
	('Transporte', 'EXPENSE');
